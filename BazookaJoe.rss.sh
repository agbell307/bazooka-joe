#!/bin/bash

SHOWLIST=/mnt/exhd1/git/bazookajoe/image.txt
SAVED_VALUE=/mnt/exhd1/git/bazookajoe/saved_value
SHOW=BazookaJoe
PODCAST_FILE=/home/aaron/html/bach/Feed$SHOW.xml

#Establish array
old_IFS=$IFS
IFS=$'\n'
lines=($(cat $SHOWLIST))
IFS=$old_IFS

#increment for each time ran
[[ -f $SAVED_VALUE ]] || echo 0 > $SAVED_VALUE
n=$(< $SAVED_VALUE)
echo $((n+1)) > $SAVED_VALUE

OUTPUT=$(cat $SAVED_VALUE)

#create feed

DATE=$(date "+%a %D") 
LENGTH=$(ls -l /mnt/exhd1/bazooka/bazookajoecomics.com/${lines[$OUTPUT]} | tail -1 | cut -d" " -f5)


f_podcast ()
{
	#Generate podcast entry and feed
echo $OUTPUT
cat > /mnt/exhd1/git/bazookajoe/BazookaJoe.txt <<- _EOF_
<item>
	<title>A Bazooka Joe Comic $DATE </title>
	<link>https://bach.bellclan.xyz/bazookajoecomics.com/${lines[$OUTPUT]}</link>
	<enclosure url="https://bach.bellclan.xyz/bazookajoecomics.com/${lines[$OUTPUT]}" length="$LENGTH" type="image/jpg" />
	<guid>$DATE ${lines[$OUTPUT]}</guid>
	<description>A $SHOW comic for $DATE</description>
</item>
_EOF_

#sed -i '24,+6d' $PODCAST_FILE 
sed -i '8r /mnt/exhd1/git/bazookajoe/BazookaJoe.txt' $PODCAST_FILE

}


if [ "$OUTPUT" -ge 3518 ];
then 
	f_podcast
	rm $SAVED_VALUE
	
else
	f_podcast
fi

